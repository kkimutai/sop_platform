# SOP Platform

Welcome to the SOP Platform! This platform is designed to help you manage your Standard Operating Procedures (SOPs) in an efficient and effective manner.

## Usage

To use the platform, simply clone the repository and checkout the `master` branch. From there, you can run the platform on your local machine.

## Authentication

To access the platform, you need to authenticate using your credentials. The authentication is handled using JSON Web Tokens (JWTs) which are issued when you login or signup.

- To login, navigate to `/auth/login` in your browser and provide your email and password. If your credentials are valid, a JWT token will be issued which you can use to access the platform.
- To signup, navigate to `/auth/signup` in your browser and provide your details, including your name, email, and password. Once you submit the form, your account will be created and you can login to access the platform.

## Admin Access

To access the admin route, navigate to `/admin` in your browser. Only admins have access to this route. If you are not an admin, you will be denied access.

As an admin, you have the ability to create, read, update, and delete SOPs. Please exercise caution when making changes to the SOPs, as these changes will affect all users of the platform.

Thank you for using the SOP Platform!
